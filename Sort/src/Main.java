import java.util.Arrays;

public class Main {

	public static void main(String[] args) {/*
		long t;
		int[] a = new int[100];
		int[] b = new int[1000];
		int[] c = new int[10000];
		int[] d = new int[100000];
		int[] e;
		Array.fill(a);
		Array.fill(b);
		Array.fill(c);
		Array.fill(d);

		e=Array.copy(a);
		t=System.currentTimeMillis();
		Sort.bubble_sort(e);
		System.out.println("Bubble sort(100): "+(System.currentTimeMillis()-t)+" ms");
		
		e=Array.copy(b);
		t=System.currentTimeMillis();
		Sort.bubble_sort(e);
		System.out.println("Bubble sort(1 000): "+(System.currentTimeMillis()-t)+" ms");

		e=Array.copy(c);
		t=System.currentTimeMillis();
		Sort.bubble_sort(e);
		System.out.println("Bubble sort(10 000): "+(System.currentTimeMillis()-t)+" ms");

		e=Array.copy(d);
		t=System.currentTimeMillis();
		Sort.bubble_sort(e);
		System.out.println("Bubble sort(100 000): "+(System.currentTimeMillis()-t)+" ms");


		System.out.println();


		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(a);
		System.out.println("Bubble sort smart(100): "+(System.currentTimeMillis()-t)+" ms");

		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(b);
		System.out.println("Bubble sort smart(1 000): "+(System.currentTimeMillis()-t)+" ms");

		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(c);
		System.out.println("Bubble sort smart(10 000): "+(System.currentTimeMillis()-t)+" ms");

		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(d);
		System.out.println("Bubble sort smart(100 000): "+(System.currentTimeMillis()-t)+" ms");


		System.out.println();


		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(a);
		System.out.println("Bubble sort smart(100 sorted): "+(System.currentTimeMillis()-t)+" ms");

		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(b);
		System.out.println("Bubble sort smart(1 000 sorted): "+(System.currentTimeMillis()-t)+" ms");

		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(c);
		System.out.println("Bubble sort smart(10 000 sorted): "+(System.currentTimeMillis()-t)+" ms");

		t=System.currentTimeMillis();
		Sort.bubble_sort_smart(d);
		System.out.println("Bubble sort smart(100 000 sorted): "+(System.currentTimeMillis()-t)+" ms");
		*/
		
		int[] data = {76,35,13,14,79,69,62,24,91,45,4,6,79,83,65,16,35,89,60,89,73,5,24,73,72,15,41,39,73,33,72,49,68,85,64,47,54,27,72,46,72,76,53,52,60,18,68,96,7,28};
		
		//System.out.println("Lomuto-Partition: "+Sort.Lomuto_Partition(Array.copy(data),0,data.length-1));
		//System.out.println("Hoare-Partition: "+Sort.Hoare_Partition(Array.copy(data),0,data.length-1));
		
		//Heap kopiec = new Heap(data);
		//kopiec.heapsort();
		//kopiec.build();
		//System.out.println(kopiec);
		
		//int[] data = {0,4,2,3,5,1,7,6,9};

		Sort.quicksort(data,0,data.length-1);
		System.out.println(Arrays.toString(data));
	}
}
