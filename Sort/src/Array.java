import java.util.Random;

public class Array {

	public static void fill(int[] array){
		Random rnd = new Random();
		for (int i=0;i<array.length;i++)
			array[i]=rnd.nextInt(1000);
	}
	
	public static int[] copy(int[] array) {
		int result[] = new int[array.length];
		for (int i=0;i<array.length;i++)
			result[i]=array[i];
		return result;
	}
	
	public static void swap(int[] a,int b,int c) {
		if (b!=c) {
			int tmp = a[b];
			a[b]=a[c];
			a[c]=tmp;
		}
	}
	
	public static void print(int[] array) {
		for (int i=0;i<100;i++)
			System.out.print(array[i]+" ");
		System.out.println();
	}
}
