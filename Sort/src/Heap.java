import java.util.Arrays;


public class Heap {

	private int data[];
	private int heapsize;
	
	public Heap(int[] data) {
		this.data = data;
	}

	
	
	@Override
	public String toString() {
		return "Heap.data=" + Arrays.toString(data);
	}



	public void heapify (int i) {
	int l = 2*i+1;
	int r = 2*i+2;
	int largest;
	if (l<=heapsize && data[l]>data[i]) largest = l;
		else largest = i; 
	if (r<=heapsize&&data[r]>data[largest]) largest = r;
	if (largest!=i) {
		Array.swap(data,i,largest);
		heapify(largest);
		}
	}
	
	public void build() {
		heapsize=data.length-1;
		for (int i = (data.length-1)/2;i>=0;i--) 
			heapify(i);
	}
	
	public void heapsort() {
		build();
		for (int i=heapsize;i>0;i--) {
			Array.swap(data,0,i);
			heapsize--;
			heapify(0);
		}
	}

	public int[] getData() {
		return data;
	}

	public void setData(int[] data) {
		this.data = data;
	}

	public int getHeapsize() {
		return heapsize;
	}

	public void setHeapsize(int heapsize) {
		this.heapsize = heapsize;
	}
	
	
	
}
