import java.util.Arrays;

public class Sort {

	public static void bubble_sort (int[] array) {
		for (int i=0;i<array.length;i++)
			for (int j=0;j<array.length-i-1;j++)
				if (array[j]>array[j+1]) Array.swap(array,j,j+1);
	}

	public static void bubble_sort_smart (int[] array) {
		boolean check;
		int tmp = 0;
		for (int i=0;i<array.length;i++) {
			check=false;
			for (int j=0;j<array.length-i-1;j++) {
				if (array[j]>array[j+1]) {
					Array.swap(array,j,j+1);
					check=true;
					tmp=j;
					break;
				}
			}
			if (check) 
				for(int j=tmp;j<array.length-i-1;j++)
					if (array[j]>array[j+1]) Array.swap(array,j,j+1);
			if (!check) break;
		}
	}

	public static int Hoare_Partition (int[] array, int p, int r) {
		int x = array[p];
		int i = p - 1;
		int j = r + 1 ;
		while (true) {
			do j--;
			while (array[j] > x);
			do i++;
			while (array[i] < x);
			if (i < j) Array.swap(array,i,j);
			else return j;
		}
	}

	public static int Lomuto_Partition (int[] array, int p, int r) {
		int x = array[r];
		int i = p-1;
		for(int j=p;j<r-1;j++) {
			if(array[j]<=x) {
				i++;
				Array.swap(array, i, j);
			}
		}
		Array.swap(array,i+1,r);
		return i+1;
	}
	
	public static void quicksort (int[] array, int a, int b) {
		int i=a;
		int j=b;
		int v=array[(a+b) / 2];
		do {
			while (array[i]<v) i++;
			while (v<array[j]) j--;
			if (i<=j) {
				Array.swap(array,i,j);
				i++;
				j--;
			}
		}
		while (i<=j);
		if (a<j) quicksort(array,a,j);
		if (i<b) quicksort(array,i,b);
		}
}